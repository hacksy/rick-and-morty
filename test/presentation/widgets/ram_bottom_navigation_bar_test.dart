import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:rick_and_morty/presentation/widgets/ram_bottom_navigation_bar.dart';

void main() {
  testWidgets('Testing bottom Navigation Bar', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: TestableBottomNavigationBar(currentScreen: 0),
      ),
    );
    final characterButtonFinder = find.text('Characters');
    final episodesButtonFinder = find.text('Episodes');
    final locationsButtonFinder = find.text('Locations');
    expect(characterButtonFinder, findsOneWidget);
    expect(episodesButtonFinder, findsOneWidget);
    expect(locationsButtonFinder, findsOneWidget);
  });
  testWidgets('Testing bottom Navigation Bar', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: TestableBottomNavigationBar(currentScreen: 0),
      ),
    );
    final bottomNavigationBarItem = find.byType(Icon);
    expect(bottomNavigationBarItem, findsNWidgets(3));
    //await tester.enterText(finder, 'aadasd');
  });

  testWidgets('Testing bottom Navigation Bar', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: TestableBottomNavigationBar(currentScreen: 0),
      ),
    );
    final characterButtonFinder = find.text('Characters');
    final episodesButtonFinder = find.text('Episodes');
    final locationsButtonFinder = find.text('Locations');
    await tester.tap(episodesButtonFinder);
    tester.pumpAndSettle(Duration(seconds: 15));
  });
}
