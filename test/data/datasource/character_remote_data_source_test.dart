import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:rick_and_morty/data/datasources/character_remote_data_source.dart';
import 'package:http/http.dart' as http;
import 'package:rick_and_morty/deprecated/entity/character.dart';

class MockClient extends Mock implements http.Client {}

void main() {
  CharacterRemoteDataSourceImpl character;
  http.Client client;
  var text;
  setUpAll(() async {
    // Hay una diferencia de ejecutar el test desde consola o desde el IDE
    //Desde IDE
    //final file = File('test_resources/characters.json');
    //Desde consola
    final file = File('../test_resources/characters.json');
    text = await file.readAsString();
  });

  setUp(() async {
    client = MockClient();
    character = CharacterRemoteDataSourceImpl(client: client);
  });

  test('Get Characters when calling characters endpoint', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response(text, 200));
    expect(await character.getCharacters(), isA<List<Character>>());
  });

  test('Get Characters when calling characters endpoint', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response(text, 200));
    List<Character> characters = await character.getCharacters();
    expect(characters.length, equals(20));
  });
  test('Get Characters when calling characters endpoint', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response(text, 200));
    List<Character> characters = await character.getCharacters();
    expect(characters.length, greaterThan(0));
  });
  test('Get Characters when calling characters endpoint', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response(text, 200));
    List<Character> characters = await character.getCharacters();
    expect(characters.length, isNot(null));
  });

  /*test('Get Characters when calling characters endpoint', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response('{}', 200));
    List<Character> characters = await character.getCharacters();
    expect(characters.length, isNot(null));
  });*/

  /*test('Get Characters when calling characters endpoint', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response('', 200));
    List<Character> characters = await character.getCharacters();
    expect(characters.length, isNot(null));
  });*/
  test('Get Characters when calling characters endpoint1', () async {
    when(client.get(CharacterRemoteDataSourceImpl.URLCharacters))
        .thenAnswer((_) async => http.Response('', 400));

    expectLater(
        () => character.getCharacters(), throwsA(isInstanceOf<Exception>()));
    
  });
}
