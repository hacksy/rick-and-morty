import 'package:flutter_test/flutter_test.dart';
import 'package:rick_and_morty/utilsX/lowercaser.dart';

void main() {
  Lowercaser lowercaser;
  setUp(() {
    //print("test");
    lowercaser = Lowercaser();
  });
  setUpAll(() {
    //print("Hola");
  });
  tearDown((){
    //print('bye');
  });
  tearDownAll((){
    //print('Adios');
  });
  group('Series of test that try to lowercase valid strings', () {
    test('Checks that String is converted to lowercase', () {
      final testPrueba = 'TestPrueba';
      final outputPrueba = 'testprueba';
      lowercaser.setString(testPrueba);
      expect(lowercaser.string, outputPrueba);
    });

    test('Checks that String is converted to lowercase', () {
      final testPrueba = 'SegundaCadena';
      final outputPrueba = 'segundacadena';
      lowercaser.setString(testPrueba);
      expect(lowercaser.string, outputPrueba);
    });

    test('Checks that String is converted to lowercase', () {
      final testPrueba = '123';
      final outputPrueba = '123';
      final lowercaser = Lowercaser();
      lowercaser.setString(testPrueba);
      expect(lowercaser.string, outputPrueba);
    });
  });

  group('Series of test that try to lowercase invalid strings', () {
    test('Checks that String is null', () {
      final testPrueba = null;
      final outputPrueba = null;
      lowercaser.setString(testPrueba);
      expect(lowercaser.string, outputPrueba);
    });
  });
}
