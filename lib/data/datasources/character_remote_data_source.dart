import 'dart:convert';

import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:http/http.dart' as http;

abstract class CharacterRemoteDatasource {
  Future<List<Character>> getCharacters();
}

class CharacterRemoteDataSourceImpl implements CharacterRemoteDatasource {
  final http.Client client;
  CharacterRemoteDataSourceImpl({this.client});
  static const URLCharacters = 'https://rickandmortyapi.com/api/character';

  Future<List<Character>> getCharacters() async {
    final response = await client.get(URLCharacters);
    if (response.statusCode == 200) {

      var contents = jsonDecode(response.body);
      return Character.fromJsonList(contents['results']);
    } else {
      print('haa');

      throw Exception('Error');
    }
  }
}
