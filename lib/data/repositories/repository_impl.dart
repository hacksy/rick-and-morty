import 'package:rick_and_morty/data/datasources/character_remote_data_source.dart';
import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';
import 'package:rick_and_morty/domain/repositories/repository.dart';

class RepositoryImpl extends Repository {
  final CharacterRemoteDatasource characterRemoteDataSource;
  RepositoryImpl({this.characterRemoteDataSource});
  @override
  Future<List<Character>> getCharacters() async {
    return characterRemoteDataSource.getCharacters();
  }

  @override
  Future<List<Episode>> getEpisodes() async{
      return <Episode>[];
  }

  @override
  Future<List<Location>> getLocations() async{
        return <Location>[];
  }
}
