import 'package:get_it/get_it.dart';
import 'package:rick_and_morty/data/datasources/character_remote_data_source.dart';
import 'package:rick_and_morty/data/repositories/repository_impl.dart';
import 'package:rick_and_morty/domain/repositories/repository.dart';
import 'package:rick_and_morty/domain/usecases/get_characters_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_episodes_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_locations_usecase.dart';
import 'package:rick_and_morty/presentation/bloc/bottom_navigation/bottom_navigation_bloc.dart';
import 'package:rick_and_morty/presentation/provider/character_model.dart';
import 'package:rick_and_morty/presentation/provider/episode_model.dart';
import 'package:rick_and_morty/presentation/provider/location_model.dart';
import 'package:http/http.dart' as http;

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerLazySingleton(() => http.Client());

  sl.registerFactory(() => BottomNavigationCubit());
  sl.registerLazySingleton<CharacterRemoteDatasource>(
      () => CharacterRemoteDataSourceImpl(client: sl()));

  sl.registerSingleton<Repository>(RepositoryImpl(
    characterRemoteDataSource: sl(),
  ));
  sl.registerSingleton(GetEpisodesUseCase(repository: sl()));
  sl.registerSingleton(GetCharactersUseCase(repository: sl()));
  sl.registerSingleton(GetLocationsUseCase(repository: sl()));
  sl.registerSingleton(CharacterModel(
    getCharactersUseCase: sl(),
    getEpisodesUseCase: sl(),
    getLocationsUseCase: sl(),
  ));
  sl.registerSingleton(LocationModel(
    getCharactersUseCase: sl(),
    getEpisodesUseCase: sl(),
    getLocationsUseCase: sl(),
  ));
  sl.registerSingleton(EpisodeModel(
    getCharactersUseCase: sl(),
    getEpisodesUseCase: sl(),
    getLocationsUseCase: sl(),
  ));
}
