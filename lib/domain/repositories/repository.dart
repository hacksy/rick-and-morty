import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';

abstract class Repository {
  Future<List<Character>> getCharacters();
  Future<List<Episode>> getEpisodes();
  Future<List<Location>> getLocations();
}
