import 'package:rick_and_morty/core/usescases/usecase.dart';
import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/domain/repositories/repository.dart';

class GetCharactersUseCase implements UseCase<List<Character>, NoParams> {
  GetCharactersUseCase({this.repository});
  final Repository repository;
  @override
  Future<List<Character>> call(NoParams params) async {
    return await repository.getCharacters();
  }
}
