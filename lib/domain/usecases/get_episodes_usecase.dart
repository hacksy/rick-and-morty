import 'package:rick_and_morty/core/usescases/usecase.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/domain/repositories/repository.dart';

class GetEpisodesUseCase implements UseCase<List<Episode>, NoParams> {
  GetEpisodesUseCase({this.repository});
  final Repository repository;
  @override
  Future<List<Episode>> call(NoParams params) async {
    return await repository.getEpisodes();
  }
}