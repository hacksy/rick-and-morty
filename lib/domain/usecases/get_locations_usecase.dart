import 'package:rick_and_morty/core/usescases/usecase.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';
import 'package:rick_and_morty/domain/repositories/repository.dart';

class GetLocationsUseCase implements UseCase<List<Location>, NoParams> {
  GetLocationsUseCase({this.repository});
  final Repository repository;
  @override
  Future<List<Location>> call(NoParams params) async {
    return await repository.getLocations();
  }
}