import 'package:flutter/widgets.dart';
import 'package:rick_and_morty/core/usescases/usecase.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/domain/usecases/get_characters_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_episodes_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_locations_usecase.dart';

class EpisodeModel extends ChangeNotifier {
  final GetCharactersUseCase getCharactersUseCase;
  final GetEpisodesUseCase getEpisodesUseCase;
  final GetLocationsUseCase getLocationsUseCase;
  List<Episode> _episodes = [];
  List<Episode> get episodes => _episodes;

  EpisodeModel({
    this.getCharactersUseCase,
    this.getEpisodesUseCase,
    this.getLocationsUseCase,
  });
  
  void getEpisodes() async {
    _episodes = await getEpisodesUseCase.call(NoParams());
    notifyListeners();
  }
}
