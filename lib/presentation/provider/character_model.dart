import 'package:flutter/widgets.dart';
import 'package:rick_and_morty/core/usescases/usecase.dart';
import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/domain/usecases/get_characters_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_episodes_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_locations_usecase.dart';

class CharacterModel extends ChangeNotifier {
  final GetCharactersUseCase getCharactersUseCase;
  final GetEpisodesUseCase getEpisodesUseCase;
  final GetLocationsUseCase getLocationsUseCase;
  List<Character> _characters = [];
  List<Character> get characters => _characters;

  CharacterModel({
    this.getCharactersUseCase,
    this.getEpisodesUseCase,
    this.getLocationsUseCase,
  });
  
  void getCharacters() async {
    _characters = await getCharactersUseCase.call(NoParams());
    notifyListeners();
  }
}
