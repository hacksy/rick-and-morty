import 'package:flutter/widgets.dart';
import 'package:rick_and_morty/core/usescases/usecase.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';
import 'package:rick_and_morty/domain/usecases/get_characters_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_episodes_usecase.dart';
import 'package:rick_and_morty/domain/usecases/get_locations_usecase.dart';

class LocationModel extends ChangeNotifier {
  final GetCharactersUseCase getCharactersUseCase;
  final GetEpisodesUseCase getEpisodesUseCase;
  final GetLocationsUseCase getLocationsUseCase;
  List<Location> _locations = [];
  List<Location> get location => _locations;

  LocationModel({
    this.getCharactersUseCase,
    this.getEpisodesUseCase,
    this.getLocationsUseCase,
  });
  
  void getLocations() async {
    _locations = await getLocationsUseCase.call(NoParams());
    notifyListeners();
  }
}
