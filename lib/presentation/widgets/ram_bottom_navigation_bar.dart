import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty/custom_icons.dart';
import 'package:rick_and_morty/presentation/bloc/bottom_navigation/bottom_navigation_bloc.dart';

class RamBottomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /*final currentScreen =
        Provider.of<BottomNavigationBloc>(context).state.currentScreen;*/
    final currentScreen =
        context.watch<BottomNavigationCubit>().state.currentScreen;
    return BottomNavigationBar(
      currentIndex: currentScreen,
      onTap: (index) =>
          context.read<BottomNavigationCubit>().changeScreen(index),
      /*   context.read<BottomNavigationBloc>().add(
              BottomNavigationLoaded(index),
            );
        Provider.of<BottomNavigationBloc>(context, listen: false).add(
          BottomNavigationLoaded(index),
        );
      },*/
      items: [
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.rick), label: 'Characters'),
        BottomNavigationBarItem(icon: Icon(CustomIcons.tv), label: 'Episodes'),
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.world), label: 'Locations'),
      ],
    );
    /*return Consumer<BottomNavigationBloc>(
      builder: (BuildContext context, BottomNavigationBloc bloc, Widget child) {
        return BottomNavigationBar(
          currentIndex: bloc.state.currentScreen,
          onTap: (index) {
            //setState(() => _currentIndex = index);
            //widget.onTap?.call(index);
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(CustomIcons.rick), label: 'Characters'),
            BottomNavigationBarItem(
                icon: Icon(CustomIcons.tv), label: 'Episodes'),
            BottomNavigationBarItem(
                icon: Icon(CustomIcons.world), label: 'Locations'),
          ],
        );
      },
    );*/
  }
}

class TestableBottomNavigationBar extends StatelessWidget {
  TestableBottomNavigationBar({this.currentScreen});
  final int currentScreen;
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentScreen,
      onTap: (index) =>
          context.read<BottomNavigationCubit>().changeScreen(index),
      items: [
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.rick), label: 'Characters'),
        BottomNavigationBarItem(icon: Icon(CustomIcons.tv), label: 'Episodes'),
        BottomNavigationBarItem(
            icon: Icon(CustomIcons.world), label: 'Locations'),
      ],
    );
  }
}
