part of 'bottom_navigation_bloc.dart';

enum BottomNavigationStatus {
  loading,
  loaded,
}

class BottomNavigationState extends Equatable {
  final BottomNavigationStatus status;
  final int currentScreen;

  const BottomNavigationState._({this.currentScreen, this.status});

  const BottomNavigationState.loading()
      : this._(status: BottomNavigationStatus.loading);
  const BottomNavigationState.loaded(screenId)
      : this._(currentScreen: screenId, status: BottomNavigationStatus.loaded);

  @override
  List<Object> get props => [currentScreen, status];
}
