import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'bottom_navigation_event.dart';
part 'bottom_navigation_state.dart';
part 'bottom_navigation_cubit.dart';

class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {
  BottomNavigationBloc() : super(BottomNavigationState.loaded(0));

  @override
  Stream<BottomNavigationState> mapEventToState(BottomNavigationEvent event) async*{
    if(event is BottomNavigationLoaded){
      yield BottomNavigationState.loaded(event.screenId);
    }
  }
}
