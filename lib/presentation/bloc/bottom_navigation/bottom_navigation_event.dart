part of 'bottom_navigation_bloc.dart';

abstract class BottomNavigationEvent extends Equatable {
  const BottomNavigationEvent();
  @override
  List<Object> get props => [];
}

class BottomNavigationLoading extends BottomNavigationEvent{}

class BottomNavigationLoaded extends BottomNavigationEvent{
  final int screenId;
  BottomNavigationLoaded(this.screenId);

  @override
  List<Object> get props => [screenId];
}