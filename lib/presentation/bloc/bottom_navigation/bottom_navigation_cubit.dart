part of 'bottom_navigation_bloc.dart';

class BottomNavigationCubit extends Cubit<BottomNavigationState>{
  BottomNavigationCubit() : super(BottomNavigationState.loaded(0));


  void changeScreen(int screenId){
    emit(BottomNavigationState.loaded(screenId));
  }
}