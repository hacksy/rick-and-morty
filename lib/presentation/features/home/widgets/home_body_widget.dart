import 'package:flutter/material.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';
import 'package:rick_and_morty/deprecated/pages/generic_list_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/presentation/bloc/bottom_navigation/bottom_navigation_bloc.dart';

import 'character_list_widget.dart';

class HomeBodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      children: [
        CharacterListWidget(),
        GenericListWidget<Episode>(),
        GenericListWidget<Location>()
      ],
      index: context.watch<BottomNavigationCubit>().state.currentScreen,
    );
  }
}