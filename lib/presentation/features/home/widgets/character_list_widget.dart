import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty/core/injection.dart';
import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/presentation/provider/character_model.dart';

class CharacterListWidget extends StatefulWidget {
  @override
  _CharacterListWidgetState createState() => _CharacterListWidgetState();
}

class _CharacterListWidgetState extends State<CharacterListWidget> {
  @override
  void initState() {
    sl<CharacterModel>().getCharacters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mqd = MediaQuery.of(context);
    var characters = Provider.of<CharacterModel>(context).characters;
    return Container(
      width: mqd.size.width,
      height: mqd.size.height,
      child: ListView.builder(
        itemCount: characters.length,
        itemBuilder: (BuildContext context, int pos) {
          Character data = characters[pos];
          bool isUri = false;
          if (data.getLeading().startsWith('https')) {
            isUri = true;
          }
          return ListTile(
            title: Text(data.getTitle()),
            subtitle: Text(data.getSubtitle()),
            leading: isUri
                ? Image.network(data.getLeading())
                : Text(data.getLeading()),
          );
        },
      ),
    );
  }
}
