import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/core/injection.dart';

import 'package:rick_and_morty/presentation/bloc/bottom_navigation/bottom_navigation_bloc.dart';
import 'package:rick_and_morty/presentation/features/home/widgets/home_body_widget.dart';
import 'package:rick_and_morty/presentation/widgets/ram_bottom_navigation_bar.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<BottomNavigationCubit>(
      create: (BuildContext context) => sl<BottomNavigationCubit>(),
      child: Scaffold(
        appBar: AppBar(
          title: Text("New Rick and Morty App"),
        ),
        body: HomeBodyWidget(),
        bottomNavigationBar: RamBottomNavigationBar(),
      ),
    );
  }
}


