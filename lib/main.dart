import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty/presentation/features/home/home_page.dart';
import 'package:rick_and_morty/presentation/provider/character_model.dart';
import 'package:rick_and_morty/presentation/provider/episode_model.dart';
import 'package:rick_and_morty/presentation/provider/location_model.dart';

import 'core/injection.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  runApp(RickAndMortyApp());
}

class RickAndMortyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => sl<CharacterModel>()),
        ChangeNotifierProvider(create: (context) => sl<EpisodeModel>()),
        ChangeNotifierProvider(create: (context) => sl<LocationModel>()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(),
      ),
    );
  }
}
