import 'package:flutter/material.dart';
import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';
import 'package:rick_and_morty/deprecated/pages/generic_list_widget.dart';
import 'package:rick_and_morty/deprecated/widgets/ram_appbar.dart';
import 'file:///D:/Projects/rick_and_morty/lib/presentation/widgets/ram_bottom_navigation_bar.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: RickAndMortyAppBar(title: "Rick and Morty"),
      bottomNavigationBar: RamBottomNavigationBar(
     /*   onTap: (index) {
          setState(() => _currentIndex = index);
        },*/
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: [GenericListWidget<Character>(), GenericListWidget<Episode>(), GenericListWidget<Location>()],
      ),
    );
  }
}
