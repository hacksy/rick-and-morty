
import 'package:flutter/material.dart';
import 'package:rick_and_morty/deprecated/data/fetch.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';

class EpisodesPage extends  StatelessWidget{
  @override
  Widget build(BuildContext context) {
    MediaQueryData mqd = MediaQuery.of(context);

    return Container(
      width: mqd.size.width,
      height: mqd.size.height,
      child: FutureBuilder<List<Episode>>(
        future: Fetcher.fetchEpisodes(),
        builder:
            (BuildContext context, AsyncSnapshot<List<Episode>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int pos) {
                  return ListTile(
                    title: Text(snapshot.data[pos].name),
                    subtitle: Text(snapshot.data[pos].airDate),
                    leading: Text(snapshot.data[pos].episode),
                  );
                });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }


}

