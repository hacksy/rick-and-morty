import 'package:flutter/material.dart';
import 'package:rick_and_morty/deprecated/data/fetch.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';

class LocationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mqd = MediaQuery.of(context);

    return Container(
      width: mqd.size.width,
      height: mqd.size.height,
      child: FutureBuilder<List<Location>>(
        future: Fetcher.fetchLocations(),
        builder:
            (BuildContext context, AsyncSnapshot<List<Location>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int pos) {
                  return ListTile(
                    title: Text(snapshot.data[pos].name),
                    subtitle: Text(snapshot.data[pos].dimension),
                    leading: Text(snapshot.data[pos].type),
                  );
                });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }


}



