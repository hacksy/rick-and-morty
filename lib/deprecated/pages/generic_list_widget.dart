import 'package:flutter/material.dart';
import 'package:rick_and_morty/deprecated/data/fetch.dart';
import 'package:rick_and_morty/deprecated/entity/base_entity.dart';


class GenericListWidget<T extends BaseEntity> extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mqd = MediaQuery.of(context);

    return Container(
      width: mqd.size.width,
      height: mqd.size.height,
      child: FutureBuilder<List<T>>(
        future: Fetcher.fetchData<T>(),
        builder: (BuildContext context, AsyncSnapshot<List<T>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int pos) {
                  T data = snapshot.data[pos];
                  bool isUri = false;
                  if (data.getLeading().startsWith('https')) {
                    isUri = true;
                  }

                  return ListTile(
                    title: Text(data.getTitle()),
                    subtitle: Text(data.getSubtitle()),
                    leading: isUri
                        ? Image.network(data.getLeading())
                        : Text(data.getLeading()),
                  );
                });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
