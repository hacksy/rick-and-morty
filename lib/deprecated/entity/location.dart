import 'base_entity.dart';

class Location extends BaseEntity{
  final int id;
  final String name;
  final String type;
  final String dimension;
  Location({this.id, this.name, this.type, this.dimension});

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      id: json['id'],
      type: json['type'],
      name: json['name'],
      dimension: json['dimension'],
    );
  }

  static List<Location> fromJsonList(List jsonList) {
    List<Location> episodes = <Location>[];

    jsonList.forEach((element) {
      episodes.add(Location.fromJson(element));
    });

    return episodes;
  }

  @override
  String getLeading() {
    return type;
  }

  @override
  String getSubtitle() {
   return dimension;
  }

  @override
  String getTitle() {
    return name;
  }
}