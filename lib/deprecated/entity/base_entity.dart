abstract class BaseEntity{
  String getTitle();
  String getSubtitle();
  String getLeading();
}