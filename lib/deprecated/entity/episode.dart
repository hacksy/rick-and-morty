import 'base_entity.dart';

class Episode extends BaseEntity{
  final int id;
  final String name;
  final String airDate;
  final String episode;
  Episode({this.id, this.name, this.airDate, this.episode});

  factory Episode.fromJson(Map<String, dynamic> json) {
    return Episode(
      id: json['id'],
      airDate: json['air_date'],
      name: json['name'],
      episode: json['episode'],
    );
  }

  static List<Episode> fromJsonList(List jsonList) {
    List<Episode> episodes = <Episode>[];

    jsonList.forEach((element) {
      episodes.add(Episode.fromJson(element));
    });

    return episodes;
  }

  @override
  String getLeading() {
    return episode;
  }

  @override
  String getSubtitle() {
    return airDate;
  }

  @override
  String getTitle() {
  return name;
  }
}
