import 'base_entity.dart';

class Character extends BaseEntity{
  final int id;
  final String name;
  final String status;
  final String image;
  Character({this.id, this.name, this.status, this.image});

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
      id: json['id'],
      status: json['status'],
      name: json['name'],
      image: json['image'],
    );
  }

  static List<Character> fromJsonList(List jsonList) {
    List<Character> characters = <Character>[];

    jsonList.forEach((element) {
      characters.add(Character.fromJson(element));
    });

    return characters;
  }

  @override
  String getLeading() {
   return image;
  }

  @override
  String getSubtitle() {
   return status;
  }

  @override
  String getTitle() {
   return name;
  }
}
