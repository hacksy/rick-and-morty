import 'package:flutter/material.dart';

class RickAndMortyAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  RickAndMortyAppBar({this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
