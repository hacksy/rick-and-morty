import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rick_and_morty/deprecated/entity/base_entity.dart';
import 'package:rick_and_morty/deprecated/entity/character.dart';
import 'package:rick_and_morty/deprecated/entity/episode.dart';
import 'package:rick_and_morty/deprecated/entity/location.dart';


class Fetcher {
  static const URLLocation = 'https://rickandmortyapi.com/api/location';
  static Future<List<Location>> fetchLocations() async {
    var response = await http.get(URLLocation);
    if (response.statusCode == 200) {
      var parsed = jsonDecode(response.body);
      return Location.fromJsonList(parsed['results']);
    }
    throw (Exception('No se pudo descargar la deprecated.data'));
  }

  static const URLEpisodes = 'https://rickandmortyapi.com/api/episode';
  static Future<List<Episode>> fetchEpisodes() async {
    var response = await http.get(URLEpisodes);
    if (response.statusCode == 200) {
      var parsed = jsonDecode(response.body);
      return Episode.fromJsonList(parsed['results']);
    }
    throw (Exception('No se pudo descargar la deprecated.data'));
  }

  static const URLCharacters = 'https://rickandmortyapi.com/api/character';
  static Future<List<Character>> fetchCharacters() async {
    var response = await http.get(URLCharacters);
    if (response.statusCode == 200) {
      var parsed = jsonDecode(response.body);
      return Character.fromJsonList(parsed['results']);
    }
    throw (Exception('No se pudo descargar la deprecated.data'));
  }

  static fetchData<T extends BaseEntity>() {
    if (T.toString() ==  'Character') {
      return fetchCharacters();
    }
    if (T.toString() ==  'Episode') {
      return fetchEpisodes();
    }
    if (T.toString() ==  'Location') {
      return fetchLocations();
    }
  }
}
