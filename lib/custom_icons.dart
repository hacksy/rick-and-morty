import 'package:flutter/widgets.dart';

class CustomIcons{

  static const IconData rick= IconData(0xe90e,fontFamily:'CustomIcons');
  static const IconData tv= IconData(0xe90f,fontFamily:'CustomIcons');
  static const IconData world= IconData(0xe900,fontFamily:'CustomIcons');

}